package com.manuel.b142.s02.s02app.services;

import com.manuel.b142.s02.s02app.models.User;

import java.util.Optional;

public interface UserService {
//    void createPost(Post newPost);
//    void updatePost(Long id, Post updatedPost);
//    void deletePost(Long id);
//    Iterable<Post> getPosts();
    void createUser(User newUser);
    void updateUser(Long id, User updatedUser);
    void deleteUser(Long id);
    Iterable<User> getUsers();
    Optional<User> findByUsername(String username);
}
